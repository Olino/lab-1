import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Монастырев","Д.Д","П-2-17", 4));
        students.add(new Student("Степанов","Д.А","П-2-17", 3));
        students.add(new Student("Леш","Д.А.","П-2-17", 5));
        students.add(new Student("Попов","В.Д.","П-2-17", 5));
        students.add(new Student("Степанова","С.А.","П-2-17", 5));
        students.add(new Student("Литков","А.С.","П-2-17", 4));
        students.add(new Student("Мирнов","В.Т.","П-2-17", 3));
        students.add(new Student("Борзенков","А.А.","П-2-17", 4));
        students.add(new Student("Дедов","А.А.","П-2-17", 2));
        students.add(new Student("Колкунова","М.П.","П-2-17", 1));

        Scanner scan = new Scanner(System.in);
        System.out.println("1 - упорядоченный / 2 - Вывыести учащихся на 4/5");
        int a = scan.nextInt();

        switch (a)
        {
            case 1:{
                for (int i = 0; i < students.size(); i++) {
                    for (int j = 0; j < i + 1; j++) {
                        if (students.get(j).getOcenka() <= students.get(i).getOcenka()) {
                            Student temp = students.get(j);
                            students.set(j, students.get(i));
                            students.set(i, temp);
                        }
                    }
                }
                for (int i = 0; i < students.size(); i++){
                    System.out.println(students.get(i).toString());
                }
                break;
            }

            case 2:{
                for (int i = 0; i < students.size(); i++) {
                    for (int j = 0; j < i + 1; j++) {
                        if (students.get(j).getOcenka() <= students.get(i).getOcenka()) {
                            Student temp = students.get(j);
                            students.set(j, students.get(i));
                            students.set(i, temp);
                        }
                    }
                }
                for (int i = 0; i < students.size(); i++){
                    if (students.get(i).getOcenka() == 4 || students.get(i).getOcenka() == 5)
                        System.out.println(students.get(i).toString());
                }
                break;
            }
        }

    }
}