public class Student {
    private String Surname, Inic, Number;
    private int Ocen;

    public Student (String Surname, String Inic, String Number, int Ocen){
        this.Surname = Surname;
        this.Inic = Inic;
        this.Number = Number;
        this.Ocen = Ocen;
    }

    public int getOcenka() {
        return Ocen;
    }

    public String toString(){
        return  String.format(   "ФИО: %s %s\nНомер группы: %s\nОценка: %d\n---", Surname, Inic, Number, Ocen);
    }
}